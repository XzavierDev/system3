# System3

System3 operating environment. Requires python3.10.

Version: 3.0: 30AAct1

FEATURES PLANNED FOR INITIAL DEVELOPER CONCEPT RELEASE:

* Working base system, that is with no snap-ins.

Notes:

To run this concept build of system3 your computer must have the following.

* Windows 10 21H1 or later (Windows 11 Beta), Avian Linux v22h1 or later, Ubuntu 20.04.2 or later, Debain 10.x based system, macOS 11.6 or later.
* 64 bit Python 3.10.0 or later (32 bit doesn't support this application).

Current Blocking issues:

* Setup is looping. - resolved

Things to add to the next beta:

* Username lock so processes can't pretend to be under another user.

