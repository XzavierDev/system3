# Copyright © 2021 Rylan Kivolski, usage subject to provided license.
# system3.py: Checks initial launch requirements and determines steps.

#Basic imports

import sys
import platform
import os

def appInfo():
    Name = "system3pyModule"
    DeveloperID = "system.core"
    Level = "Core"
    Version = "1"
    SupportedOS = {"All"}
    RequiredFeatures = {"base"}

def checkBaseRequirements():
    #check python version

    pyVer = sys.version_info
    if pyVer[0] == 3 and pyVer[1] >= 10:
        pass
    else:
        print("Sorry. This application requires Python 3.10rc1 or above.")
        exit()

    #check platform architecture

    sysType = platform.architecture()[0]
    if sysType != "64bit":
        print("Sorry. You must use 64 bit Python to run this application.")
    else:
        pass
    print("system3_launcher: Machine is compatible.")
    return True

def determineSetupRequirement():
    #find the directory this file is in
    rootDirectory = os.path.dirname(os.path.realpath(__file__))

    #try and load in preferences, if fail load setup

    try:
        basePrefsFile = open(rootDirectory+os.sep+"base_system"+os.sep+"registry"+os.sep+"system3_core"+os.sep+"basePrefs.s3p", "rt")
        basePrefsFile.close()

        sys.path.append(rootDirectory+os.sep+"base_system")

    
    except FileNotFoundError:
        print("Base preferences not found. Launching setup.")

        sys.path.append(rootDirectory+os.sep+"system3"+os.sep+"base_system")
        print (rootDirectory+os.sep+"system3"+os.sep+"base_system")

        #import our OE kernel and tell it to enter setup

        import oekernel as k
        k.loader()







def execMe():
    checkBaseRequirements()
    determineSetupRequirement()

execMe()

