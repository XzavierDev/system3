# Copyright © 2021 Rylan Kivolski, usage subject to provided license.
# oekernel.py: File that event handles, starts tasking services etc.

#Basic imports

import sys
import platform
import os

def appInfo():
    Name = "OEKernelpyModule"
    DeveloperID = "system.core"
    Level = "Core"
    Version = "1"
    SupportedOS = {"All"}
    RequiredFeatures = {"base"}

def setupNow():
    #Here is where we generate base preference file

    answers = []

    print("Welcome to System3 Setup. Please answer the following questions.\n")

    #This will preconfigure setup values for automated testing and enterprise deployment scenarios. It is ignored for now.
    print("If you have a setup key, enter it now, otherwise - leave blank.")
    preDeterminedSetup = input("> ")

    print("\nSome setup values are fixed during development. These will be printed where the question would be.")

    print("usermodeconfiguration: system3")
    

    #set base directory

    baseDirectory = os.path.dirname(os.path.realpath(__file__))

    while True:

        print("Enter a username")
        userName = input("> ")

        try:
            #make directory if not exists
            userDirectory = baseDirectory.replace("base_system","users"+os.sep+"system3")
            os.mkdir(userDirectory+os.sep+userName)
        except FileExistsError:
            print("User exists. Please enter another name.")
            continue

        #set up questions for enabled features

        print("system3 can function as a workstation or a server. Workstations typically only have the desktop shell module installed.")
        print("Core components are enabled by default. Would you like to enable...")

        answers.append("usermodeconfiguration: system3")

        answersBefore = answers

        while True:
            print("Full Desktop Shell Module?")
            optional = input("(Y/N)> ")
            if optional != "y" and optional != "n":
                answers = answersBefore
                continue
            else:
                if optional == "y":
                    answers.append("DesktopShell: yes")
                else:
                    answers.append("DesktopShell: no")

            print("Remote Access Components?")
            optional = input("(Y/N)> ")
            if optional != "y" and optional != "n":
                answers = answersBefore
                continue
            else:
                if optional == "y":
                    answers.append("RemoteAccess: yes")
                else:
                    answers.append("RemoteAccess: no")

            print("Team Infrastructure Stack?")
            optional = input("(Y/N)> ")
            if optional != "y" and optional != "n":
                answers = answersBefore
                continue
            else:
                if optional == "y":
                    answers.append("TeamInfra: yes")
                else:
                    answers.append("TeamInfra: no")

            print("system1 (AIOE) v1.0.5 application subsystem?")
            optional = input("(Y/N)> ")
            if optional != "y" and optional != "n":
                answers = answersBefore
                continue
            else:
                if optional == "y":
                    answers.append("s1s: yes")
                else:
                    answers.append("s1s: no")

            #see if user wants local admin console disabled, not yet implemented

            print("Do you want to disable the local admin console after first logon?")
            print("NOT IMPLEMENTED")
            break #fails here, loops
        break
    
    #Values for the system

    sVals = []
    sVals.append("Preference File Version: 30\n")
    sVals.append("Software Version: 3.0.0.0.t.1\n")

    print("Writing preferences. Do not close this window...")
    prefFile = open(baseDirectory+os.sep+"registry"+os.sep+"system3_core"+os.sep+"system3.cfg", "wt")
    completeVals = sVals + answers
    for item in completeVals:
        prefFile.write(item+"\n")
    prefFile.close()
    print("Setup complete. Press return to load the software now, otherwise CTRL C to exit.")
    cont = input("...")

def loader():
    print("Welcome to System3. Copyright © 2021 XzavierDev, usage subject to license provided.")
    print("STATUS: Loading system preference values into RAM...")
    import transientmemory as ram

    #Use the new match case system in 3.10 for clean syntax
    osCheckResult = platform.system()
    for oscheckResult in ["Windows","Darwin","Linux"]:
        match osCheckResult:
            case "Windows":
                ram.core_current_os_basic = "Windows NT"
            case "Darwin":
                ram.core_current_os_basic = "macOS"
            case "Linux":
                ram.core_current_os_basic = "Linux Based OS"
            case _:
                ram.core_current_os_basic = "Unknown (assumed *NIX variant)"

    
    #read preference file

    #fix base directory
    baseDirectory = os.path.dirname(os.path.realpath(__file__))

    try:
        prefFile = open(baseDirectory+os.sep+"registry"+os.sep+"system3_core"+os.sep+"system3.cfg", "rt")
        fileData = prefFile.readlines()
        prefFile.close()

    except FileNotFoundError:
        print("CE-OEKERNEL-1")
        setupNow()

    for fileData in ["DesktopShell: yes","RemoteAccess: yes","TeamInfra: yes"]:
        match fileData:
            case "DesktopShell: yes":
                ram.optional_stacks.append("DesktopShell")
            case "RemoteAccess: yes":
                ram.optional_stacks.append("RemoteAccess")
            case "TeamInfra: yes":
                ram.optional_stacks.append("TeamInfra")
            case _:
                pass

    if "DesktopShell" in ram.optional_stacks:
        ram.optional_admin_console_disabled_is_desktop = True
    else:
        pass

    print("STATUS: RAM populated. Starting tasker.")
    import tasker
    tasker.initialise()
    








def execMe():

    if os.path.exists(baseDirectory+os.sep+"registry"+os.sep+"system3_core"+os.sep+"basePrefs.s3p"):
        loader()
    else:
        setupNow()
    loader()

    