# Copyright © 2021 Rylan Kivolski, usage subject to provided license.
# transientmemory.py: File that holds transient variables and makes them available to processes.

#This module is specially exempt from execMe() because of loading constraints

#Basic imports

import sys
import platform
import os

def appInfo():
    Name = "TransientMemorypyModule"
    DeveloperID = "system.core"
    Level = "Core"
    Version = "1"
    SupportedOS = {"All"}
    RequiredFeatures = {"base"}

#Here we store all the core variables...

core_usermode = "system3" #required for now - don't change
core_current_username = ""
core_current_os_basic = ""

#what optional components are enabled...

optional_stacks = []

#if admin console is disabled...

optional_admin_console_is_disabled_after_firstrun = False
optional_admin_console_disabled_is_desktop = False

