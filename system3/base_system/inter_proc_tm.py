# Copyright © 2021 Rylan Kivolski, usage subject to provided license.
# inter_proc_tm.py: File that holds transient variables and makes them modifiable to processes and subprocesses.

#This module is specially exempt from execMe() because of loading constraints

#Basic imports

import sys
import platform
import os

def appInfo():
    Name = "InterProcTMPyModule"
    DeveloperID = "system.core"
    Level = "Core"
    Version = "1"
    SupportedOS = {"All"}
    RequiredFeatures = {"base"}

pass

#We need to work out a way to communicate between processes and then use this for some stuff.

